/**
 * \file vector.h
 * \brief Contains all vector routine headers (OLD VERSION).
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 23 January 2020
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef VECTOR_H
	#define VECTOR_H
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>
	#include <errno.h>
	#include <string.h>

	#define VECTOR(TYPE, NAME) \
		typedef struct { \
			TYPE * value; \
			size_t cardinal; \
			size_t capacity; \
		} Vector##NAME;

	#define VECTOR_MALLOC(TYPE, NAME) \
		static Vector##NAME * Vector##NAME##_malloc(size_t capacity) \
		{ \
			return (Vector##NAME *)Vector_malloc(capacity, sizeof(TYPE)); \
		}
	
	#define VECTOR_REALLOC(TYPE, NAME) \
		static Vector##NAME * Vector##NAME##_realloc(Vector##NAME * vector, size_t capacity) \
		{ \
			return (Vector##NAME *)Vector_realloc((Vector *)vector, capacity, sizeof(TYPE)); \
		}

	#define VECTOR_AT(TYPE, NAME) \
		static TYPE * Vector##NAME##_at(Vector##NAME * vector, size_t index) \
		{ \
			return (TYPE *)Vector_at((Vector *)vector, index, sizeof(TYPE)); \
		}

	#define VECTOR_GET(TYPE, NAME) \
		static TYPE Vector##NAME##_get(Vector##NAME * vector, size_t index) \
		{ \
			return *((TYPE *)Vector_at((Vector *)vector, index, sizeof(TYPE))); \
		}

	#define VECTOR_SET(TYPE, NAME) \
		static void Vector##NAME##_set(Vector##NAME * vector, size_t index, TYPE value) \
		{ \
			*((TYPE *)Vector_at((Vector *)vector, index, sizeof(TYPE))) = value; \
		}

	#define VECTOR_RIGHT_SHIFT(TYPE, NAME) \
		static void Vector##NAME##_right_shift(Vector##NAME * vector) \
		{ \
			Vector_right_shift((Vector *)vector, sizeof(TYPE)); \
		}

	#define VECTOR_LEFT_SHIFT(TYPE, NAME) \
		static void Vector##NAME##_left_shift(Vector##NAME * vector) \
		{ \
			Vector_left_shift((Vector *)vector, sizeof(TYPE)); \
		}

	#define VECTOR_CMP(TYPE, NAME) \
		static bool Vector##NAME##_cmp(Vector##NAME * vector0, Vector##NAME * vector1) \
		{ \
			return Vector_cmp((Vector *)vector0, sizeof(TYPE), (Vector *)vector1, sizeof(TYPE)); \
		}
	
	#define VECTOR_ISIN(TYPE, NAME) \
		static bool Vector##NAME##_isin(Vector##NAME * vector, TYPE value) \
		{ \
			return Vector_isin((Vector *)vector, &value, sizeof(TYPE)); \
		}

	#define VECTOR_COUNT(TYPE, NAME) \
		static size_t Vector##NAME##_count(Vector##NAME * vector, TYPE value) \
		{ \
			return Vector_count((Vector *)vector, &value, sizeof(TYPE)); \
		}

	#define VECTOR_COUNT_OVERLAP(TYPE, NAME) \
		static size_t Vector##NAME##_count_overlap(Vector##NAME * vector, TYPE value) \
		{ \
			return Vector_count_overlap((Vector *)vector, &value, sizeof(TYPE)); \
		}
	
	#define VECTOR_PUSH_BACK(TYPE, NAME) \
		static void Vector##NAME##_push_back(Vector##NAME * vector, TYPE value) \
		{ \
			Vector_push_back((Vector *)vector, &value, sizeof(TYPE)); \
		}

	#define VECTOR_POP_BACK(TYPE, NAME) \
		static void Vector##NAME##_pop_back(Vector##NAME * vector) \
		{ \
			Vector_pop_back((Vector *)vector, sizeof(TYPE)); \
		}

	#define VECTOR_PUSH_FRONT(TYPE, NAME) \
		static void Vector##NAME##_push_front(Vector##NAME * vector, TYPE value) \
		{ \
			Vector_push_front((Vector *)vector, &value, sizeof(TYPE)); \
		}

	#define VECTOR_POP_FRONT(TYPE, NAME) \
		static void Vector##NAME##_pop_front(Vector##NAME * vector) \
		{ \
			Vector_pop_front((Vector *)vector, sizeof(TYPE)); \
		}

	typedef struct {
		void * value;
		size_t cardinal;
		size_t capacity;
	} Vector;

	typedef unsigned char Byte;

	/**
	 * \fn Vector * Vector_malloc(const size_t capacity, const size_t offset)
	 * \brief Allocates a vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param capacity Number of elements.
	 * \param offset Size of one element (in byte).
	 * \return Address of a vector object.
	 */
	Vector * Vector_malloc(const size_t capacity, const size_t offset);

	/**
	 * \fn Vector * Vector_realloc(Vector * const vector, const size_t capacity, const size_t offset)
	 * \brief Reallocates a vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param capacity New capacity of the given vector.
	 * \param offset Size in byte of an element in the vector.
	 * \return NULL if error occurred or if capacity is 0 else the address of the reallocated vector.
	 */
	Vector * Vector_realloc(Vector * const vector, const size_t capacity, const size_t offset);

	/**
	 * \fn void * Vector_free(void * vectorAddress)
	 * \brief Frees previously allocated vector
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2019
	 * \param vectorAddress Address of vector to be freed.
	 */
	void * Vector_free(void * vectorAddress);

	/**
	 * \fn bool Vector_isempty(const Vector * const vector)
	 * \brief Tests if a vector is empty.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \return true if no elements are written in vector else false. 
	 */
	bool Vector_isempty(const Vector * const vector);

	/**
	 * \fn bool Vector_isspaceless(const Vector * const vector)
	 * \brief Tests if a vector is spaceless.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \return true if vector takes no space in memory else false.
	 */
	bool Vector_isspaceless(const Vector * const vector);

	/**
	 * \fn void * Vector_at(Vector * const vector, const size_t index, const size_t offset)
	 * \brief Returns address of an element of given index.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param index Given index of an element in the vector.
	 * \param offset Size in byte of an element in the vector.
	 * \return Address of the element in vector of given index.
	 */
	void * Vector_at(Vector * const vector, const size_t index, const size_t offset);

	/**
	 * \fn void Vector_right_shift(void * const source, const size_t cardinal, const size_t offset)
	 * \brief Shifts all vector value(s) from one element size to the right.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param source Address of the source to shift. (corresponds to vector->value field).
	 * \param cardinal Number of elements to shift (corresponds to vector->cardinal field).
	 * \param offset size of one element (in byte).
	 */
	void Vector_right_shift(Vector * const vector, const size_t offset);

	/**
	 * \fn void Vector_left_shift(void * const source, const size_t cardinal, const size_t offset)
	 * \brief Shifts all vector value(s) from one element size to the left.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param source Address of the source to shift. (corresponds to vector->value field).
	 * \param cardinal Number of elements to shift (corresponds to vector->cardinal field).
	 * \param offset Size of one element (in byte).
	 */
	void Vector_left_shift(Vector * const vector, const size_t offset);
	
	/**
	 * \fn bool Vector_cmp(const Vector * const vector0, const size_t offset0, const Vector * const vector1, const size_t offset1);
	 * \brief Compares two given vectors.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 january 2020
	 * \param vector0 A given vector.
	 * \param offset0 Offset of \a vector0.
	 * \param vector1 A given vector.
	 * \param offset1 Offset of \a vector1.
	 * \return true if \a vector0 and \a vector1 contain same element(s) in same order else false
	 */
	bool Vector_cmp(const Vector * const vector0, const size_t offset0, const Vector * const vector1, const size_t offset1);

	/**
	 * \fn bool Vector_isin(const Vector * const vector, const void * const sequence, const size_t offset)
	 * \brief Tests if a given element is in a given vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param sequence Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return true if a given element is in a given vector else false.
	 */
	bool Vector_isin(const Vector * const vector, const void * const sequence, const size_t offset);

	/**
	 * \fn size_t Vector_count(const Vector * const vector, const void * const sequence, const size_t offset)
	 * \brief Counts occurrences of a given element in a given vector with a variable jump of given offset size.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param given Vector.
	 * \param sequence Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return Number of occurrences.
	 */
	size_t Vector_count(const Vector * const vector, const void * const sequence, const size_t offset);

	/**
	 * \fn size_t Vector_count(const Vector * const vector, const void * const sequence, const size_t offset)
	 * \brief Counts occurrences of a given element in a given vector with a constant jump of one byte. 
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param sequence Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 * \return number of occurrences
	 */
	size_t Vector_count_overlap(const Vector * const vector, const void * const sequence, const size_t offset);

	/**
	 * \fn void Vector_push_back(Vector * const vector, const void * const value, const size_t offset)
	 * \brief Adds an element behind the last element, reallocates the vector if not enough space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param value Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_push_back(Vector * const vector, const void * const value, const size_t offset);

	/**
	 * \fn void Vector_pop_back(Vector * const vector, const size_t offset)
	 * \brief removes the last element of the vector, if the vector is empty but not space less then it reallocates the vector of -1 element space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_pop_back(Vector * const vector, const size_t offset);

	/**
	 * \fn void Vector_push_front(Vector * const vector, const void * const value, const size_t offset)
	 * \brief Adds an element in front of the first element, reallocates the vector if not enough space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param value Address of a given element.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_push_front(Vector * const vector, const void * const value, const size_t offset);

	/**
	 * \fn void Vector_pop_front(Vector * const vector, const size_t offset)
	 * \brief Removes the first element of the vector, if the vector is empty but not space less then it reallocates the vector of -1 element space.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param vector Given vector.
	 * \param offset Size in byte of an element in the vector.
	 */
	void Vector_pop_front(Vector * const vector, const size_t offset);

#endif
