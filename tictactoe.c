/**
 * \file titactoe.c
 * \brief Contains tictactoe routines.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 04 February 2020
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "tictactoe.h"

long randint(long min, long max)
{
	time_t t;
	time(&t);
	srandom(t);
	return min + (random() % (max - min + 1));
}

Cell * tictactoe_cell_allocinit(void)
{
	Cell * cell;
	cell = malloc(sizeof(Cell));
	if (cell == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	*cell = EMPTY;
	return cell;
}

VECTOR_MALLOC(Cell *, Cell)
VECTOR_MALLOC(VectorCell *, Grid)

VectorGrid * tictactoe_grid_allocinit(const size_t base)
{
	VectorGrid * grid;
	size_t i, j;
	grid = VectorGrid_malloc(2 * (base + 1));
	for (i = 0; i < base; i++) {
		grid->value[i] = VectorCell_malloc(base);
		for (j = 0; j < base; j++) {
			grid->value[i]->value[j] = tictactoe_cell_allocinit();
		}
	}
	for (i = 0; i < base; i++) {
		grid->value[base + i] = VectorCell_malloc(base);
		for (j = 0; j < base; j++) {
			grid->value[base + i]->value[j] = grid->value[j]->value[i];
		}
	}
	grid->value[2 * base] = VectorCell_malloc(base);
	for (i = 0; i < base; i++) {
		grid->value[2 * base]->value[i] = grid->value[i]->value[i];
	}
	grid->value[2 * base + 1] = VectorCell_malloc(base);
	for (i = 0; i < base; i++) {
		grid->value[2 * base + 1]->value[i] = grid->value[i]->value[base - (i + 1)];
	}
	return grid;
}

VectorGrid * tictactoe_grid_free(VectorGrid * const grid)
{
	size_t i, j;
	for (i = 0; i < grid->capacity; i++) {
		for (j = 0; j < grid->value[i]->capacity; j++) {
			free(grid->value[i]->value[j]);
			grid->value[i]->value[j] = NULL;
		}
		grid->value[i] = Vector_free(grid->value[i]);
	}
	return Vector_free(grid);
}

void tictactoe_grid_display(VectorGrid * const grid)
{
	size_t i, j, gridLenght;
	gridLenght = grid->value[0]->capacity;
	i = 0;
	while ((i < gridLenght-1) && (i < 10)) {
		printf("%lu ", i);
		i++;
	}
	for (; i < gridLenght-1; i++) {
		printf(". ");
	}
	printf("%lu\n", gridLenght-1);
	for (i = 0; i < gridLenght; i++) {
		for (j = 0; j < gridLenght; j++) {
			printf("%c ", (char)*grid->value[i]->value[j]);
		}
		printf("%lu\n", i);
	}
}

size_t tictactoe_get_rectangular_line_index(const Coordinate coord)
{
	return coord.y;
}

size_t tictactoe_get_rectangular_col_index(const VectorGrid * const grid, const Coordinate coord)
{
	return coord.y + coord.x + (grid->value[0]->capacity - coord.y);
}

size_t tictactoe_get_rectangular_descending_diagonal_index(const VectorGrid * const grid, const Coordinate coord)
{
	return coord.y - coord.x + 2 * grid->value[0]->capacity;
}

size_t tictactoe_get_rectangular_ascending_diagonal_index(const VectorGrid * const grid, const Coordinate coord)
{
	return coord.y - (grid->value[0]->capacity - (coord.x + 1)) + (2 * grid->value[0]->capacity) + 1;
}

Coordinate tictactoe_convert_rectangular_to_square_coord(const VectorGrid * const grid, const Coordinate rectCoord)
{
	Coordinate squareCoord;
	ssize_t gridLenght;
	gridLenght = grid->value[0]->capacity;
	if (rectCoord.y < gridLenght) {
		squareCoord = rectCoord;
	} else if (rectCoord.y < (2 * gridLenght)) {
		squareCoord.y = rectCoord.x;
		squareCoord.x = rectCoord.y - gridLenght;
	} else if (rectCoord.y == (2 * gridLenght)) {
		squareCoord.y = rectCoord.x;
		squareCoord.x = rectCoord.x;
	} else {
		squareCoord.y = rectCoord.x;
		squareCoord.x = gridLenght - (rectCoord.x + 1);
	}
	return squareCoord;
}

bool tictactoe_is_complete(const VectorCell * const cells, const Cell symbol)
{
	size_t i;
	i = 0;
	while ((i < cells->capacity) && (*cells->value[i] == symbol)) {
		i++;
	}
	return i == cells->capacity;
}

bool tictactoe_isthere_winner(const VectorGrid * const grid, Coordinate lastMove)
{
	Cell value;
	bool isThereWinner;
	size_t gridLenght;
	gridLenght = grid->value[0]->capacity;
	value = *grid->value[lastMove.y]->value[lastMove.x];
	isThereWinner = tictactoe_is_complete(grid->value[tictactoe_get_rectangular_line_index(lastMove)], value);
	isThereWinner = isThereWinner || tictactoe_is_complete(grid->value[tictactoe_get_rectangular_col_index(grid, lastMove)], value);
	if (tictactoe_get_rectangular_descending_diagonal_index(grid, lastMove) == (2 * gridLenght)) {
		isThereWinner = isThereWinner || tictactoe_is_complete(grid->value[2 * gridLenght], value);
	}
	if (tictactoe_get_rectangular_ascending_diagonal_index(grid, lastMove) == (2 * gridLenght + 1)) {
		isThereWinner = isThereWinner || tictactoe_is_complete(grid->value[2 * gridLenght + 1], value);
	}
	return isThereWinner;
}

Segment tictactoe_determine_segment(const VectorGrid * const grid, const size_t rectangularY, const Cell playerSymbol)
{
	Segment segment;
	VectorCell * cells;
	ssize_t i, symbolOccurrence;
	segment.gridIndex.y = rectangularY;
	cells = grid->value[rectangularY];
	segment.nextMove.y = -1;
	segment.nextMove.x = -1;
	symbolOccurrence = 0;
	i = 0;
	while ((i < (ssize_t)cells->capacity) && (symbolOccurrence != -1)) {
		if (*grid->value[rectangularY]->value[i] == playerSymbol) {
			symbolOccurrence++;
		} else if (*grid->value[rectangularY]->value[i] != EMPTY) {
			symbolOccurrence = -1;
		}
		i++;
	}
	segment.fillingLevel = symbolOccurrence;
	return segment;
}

VECTOR_MALLOC(size_t, SizeT)
VECTOR_PUSH_BACK(size_t, SizeT)

Segment tictactoe_determine_optimal_orientation(const VectorGrid * grid, const Coordinate previousMove)
{
	Segment optimalSegment, currSegment;
	Cell playerSymbol;
	VectorSizeT * indexes;
	size_t i, gridLenght;
	indexes = VectorSizeT_malloc(4);
	playerSymbol = *grid->value[previousMove.y]->value[previousMove.x];
	gridLenght = grid->value[0]->capacity;
	VectorSizeT_push_back(indexes, tictactoe_get_rectangular_line_index(previousMove));
	VectorSizeT_push_back(indexes, tictactoe_get_rectangular_col_index(grid, previousMove));
	if ((i = tictactoe_get_rectangular_descending_diagonal_index(grid, previousMove)) == (2 * gridLenght)) {
		VectorSizeT_push_back(indexes, i);
	}
	if ((i = tictactoe_get_rectangular_ascending_diagonal_index(grid, previousMove)) == (2 * gridLenght + 1)) {
		VectorSizeT_push_back(indexes, i);
	}
	optimalSegment = tictactoe_determine_segment(grid, indexes->value[0], playerSymbol);
	for (i = 1; i < indexes->cardinal; i++) {
		currSegment = tictactoe_determine_segment(grid, indexes->value[i], playerSymbol);
		if (optimalSegment.fillingLevel < currSegment.fillingLevel) {
			optimalSegment = currSegment;
		}
	}
	indexes = Vector_free(indexes);
	return optimalSegment;
}

size_t tictactoe_count_symbol(const VectorCell * const cells, const Cell symbol)
{
	size_t i, occurrence;
	occurrence = 0;
	i = 0;
	while (i < cells->capacity) {
		if (*cells->value[i] == symbol) {
			occurrence++;
		} else if (*cells->value[i] != EMPTY) {
			occurrence = 0;
			i = cells->capacity;
		}
		i++;
	}
	return occurrence;
}

size_t tictactoe_determine_opponent_filling_level(const VectorGrid * const grid, const size_t rectangularY, const Coordinate placement, const Cell opponentSymbol)
{
	size_t fillingLevel, gridLenght;
	gridLenght = grid->value[0]->capacity;
	fillingLevel = 0;
	if (rectangularY < gridLenght) {
		fillingLevel += tictactoe_count_symbol(grid->value[tictactoe_get_rectangular_col_index(grid, placement)], opponentSymbol);
	} else if (rectangularY < (2 * gridLenght)) {
		fillingLevel += tictactoe_count_symbol(grid->value[tictactoe_get_rectangular_line_index(placement)], opponentSymbol);
	} else {
		fillingLevel += tictactoe_count_symbol(grid->value[tictactoe_get_rectangular_col_index(grid, placement)], opponentSymbol);
		fillingLevel += tictactoe_count_symbol(grid->value[tictactoe_get_rectangular_line_index(placement)], opponentSymbol);
	}
	return fillingLevel;
}

void tictactoe_determine_optimal_placement(const VectorGrid * const grid, Segment * const segment, const Cell opponentSymbol, const Cell playerSymbol)
{
	Coordinate currPlacement, optimalPlacement;
	VectorCell * cells;
	size_t i;
	ssize_t currFilling, optimalFilling;
	if (segment->fillingLevel != -1) {
		cells = grid->value[segment->gridIndex.y];
		segment->gridIndex.x = 0;
		optimalPlacement = segment->nextMove;
		optimalFilling = -1;
		for (i = 0; i < cells->capacity; i++) {
			if (*cells->value[i] != playerSymbol) {
				segment->gridIndex.x = i;
				currPlacement = tictactoe_convert_rectangular_to_square_coord(grid, segment->gridIndex);
				currFilling = tictactoe_determine_opponent_filling_level(grid, segment->gridIndex.y, currPlacement, opponentSymbol);
				if (optimalFilling < currFilling) {
					optimalPlacement = currPlacement;
					optimalFilling = currFilling;
				}
			}
		}
		segment->nextMove = optimalPlacement;
	}
}

Segment tictactoe_determine_fullest_segment(const VectorGrid * const grid, const VectorCoord * const history, const Cell opponentSymbol)
{
	Segment fullestSegment, currSegment;
	size_t i;
	Cell playerSymbol;
	playerSymbol = *grid->value[history->value[0].y]->value[history->value[0].x];
	fullestSegment = tictactoe_determine_optimal_orientation(grid, history->value[0]);
	for (i = 1; i < history->cardinal; i++) {
		currSegment = tictactoe_determine_optimal_orientation(grid, history->value[i]);
		if (fullestSegment.fillingLevel < currSegment.fillingLevel) {
			fullestSegment = currSegment;
		}
	}
	tictactoe_determine_optimal_placement(grid, &fullestSegment, opponentSymbol, playerSymbol);
	return fullestSegment;
}

bool tictactoe_are_equal_coordinate(Coordinate c0, Coordinate c1)
{
	return c0.y == c1.y && c0.x == c1.x;
}

Coordinate tictactoe_determine_first_move(const VectorGrid * const grid)
{
	Coordinate firstMove, middleMove;
	size_t i, j, gridLenght;
	gridLenght = grid->value[0]->capacity;
	middleMove.y = gridLenght/2;
	middleMove.x = gridLenght/2;
	firstMove = middleMove;
	if (*grid->value[firstMove.y]->value[firstMove.x] != EMPTY) {
		i = 0;
		while (tictactoe_are_equal_coordinate(firstMove, middleMove)) {
			j = 0;
			while ((j < gridLenght) && tictactoe_are_equal_coordinate(firstMove, middleMove)) {
				if (*grid->value[i]->value[j] == EMPTY) {
					firstMove.y = i;
					firstMove.x = j;
				}
				j += gridLenght - 1;
			}
			i += gridLenght - 1;
		}
	}
	return firstMove;
}

void tictactoe_grid_alter(VectorGrid * const grid, const Coordinate move, const Cell symbol)
{

	if (move.y == -1) {
		puts("END GAME.");
		puts("DRAW NO MORE POSSIBILITIES FOUND.");
		exit(EXIT_SUCCESS);
	}
	*grid->value[move.y]->value[move.x] = symbol;
}

VECTOR_MALLOC(Coordinate, Coord)
VECTOR_PUSH_BACK(Coordinate, Coord)

Coordinate tictactoe_machine_play(VectorGrid * const grid, VectorCoord * const botHistory, const VectorCoord * const humanHistory)
{
	Coordinate move;
	Segment defendSegment, attackSegment;
	ssize_t winFill;
	winFill = grid->value[0]->capacity - 1;
	if (humanHistory->cardinal == 0) {
		move.y = grid->value[0]->capacity/2;
		move.x = grid->value[0]->capacity/2;
	} else if (botHistory->cardinal == 0) {
		move = tictactoe_determine_first_move(grid);
	} else {
		defendSegment = tictactoe_determine_fullest_segment(grid, humanHistory, BOT);
		attackSegment = tictactoe_determine_fullest_segment(grid, botHistory, HUMAN);
		move = attackSegment.nextMove;
		if (((defendSegment.fillingLevel == (winFill)) && (attackSegment.fillingLevel < winFill)) || move.y == -1) {
			move = defendSegment.nextMove;
		}
	}
	VectorCoord_push_back(botHistory, move);
	tictactoe_grid_alter(grid, move, BOT);
	printf("\nbot choice: (y, x) -> (%lu, %lu)\n\n", move.y, move.x);
	return move;
}

int secure_inputint(char * inputMsg)
{
	char * buff;
	size_t size;
	ssize_t ret, i;
	int result;
	buff = NULL;
	size = 0;
	do {
		printf(inputMsg);
		ret = getline(&buff, &size, stdin);
		if (ret == -1) {
			fprintf(stderr, "error: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		i = 0;
		while ((buff[i] >= 48) && (buff[i] <= 57)) {
			i++;
		}
	} while (i != (ret - 1));
	sscanf(buff, "%d", &result);
	free(buff);
	return result;
}

Coordinate tictactoe_get_humain_input(VectorGrid * const grid, VectorCoord * const humanHistory)
{
	Coordinate move;
	bool validInput;
	size_t gridLenght, x, y;
	gridLenght = grid->value[0]->capacity;
	do {
		printf("player choice:\n");
		y = secure_inputint("Y: ");
		x = secure_inputint("X: ");
		validInput = (y < gridLenght) && (x < gridLenght) && (*grid->value[y]->value[x] == EMPTY);
		if (! validInput) {
			printf("no cell available at this position, try again ...\n");
		}
	} while (! validInput);
	move.y = y;
	move.x = x;
	VectorCoord_push_back(humanHistory, move);
	tictactoe_grid_alter(grid, move, HUMAN);
	return move;
}

bool tictactoe_play_as(VectorGrid * const grid, bool playerId, VectorCoord * const botHistory, VectorCoord * const humanHistory)
{
	Coordinate previousMove;
	bool isWinner;
	if (playerId) {
		previousMove = tictactoe_get_humain_input(grid, humanHistory);
	} else {
		previousMove = tictactoe_machine_play(grid, botHistory, humanHistory);
	}
	tictactoe_grid_display(grid);
	isWinner = tictactoe_isthere_winner(grid, previousMove);
	if (isWinner) {
		if (playerId) {
			puts("HUMAN WON!");
		} else {
			puts("BOT WON!");
		}
	}
	return isWinner;
}

void tictactoe_print_help(void) {
	printf("WELCOME !\n\nYou're about to play the famous tictactoe game and you should be aware that:\n\n");
	printf("- You're about to play against an unbeatable BOT no matter what grid size you choose (except for 2*2 and 1*1), it will either try to win or stop you from winning.\n\n");
	printf("- The first player to start moving is determined randomly.\n\n");
	printf("- Your symbol is '0', the BOT symbol is '+' and an empty cell is represented by '~' symbol.\n\n");
	printf("- You play by entering the Y and X coordinates of the cell in which you wanna put your symbol.\n\n");
	printf("- Every time the BOT plays, you will be noticed of where the move occurred.\n\n");
	printf("- Enjoy !!\n\n");
}

size_t tictactoe_get_gridlenght(){
	size_t gridLenght;
	do {
		gridLenght = secure_inputint("Enter the grid size: ");
		if (gridLenght == 0) {
			printf("Invalid. ");
		}
	} while (gridLenght == 0);
	return gridLenght;
}

void tictactoe_start(void)
{
	VectorGrid * grid;
	VectorCoord * humanHistory, * botHistory;
	size_t i, gridLenght;
	bool isWinner, playerId;
	tictactoe_print_help();
	gridLenght = tictactoe_get_gridlenght();
	grid = tictactoe_grid_allocinit(gridLenght);
	humanHistory = VectorCoord_malloc(((gridLenght*gridLenght)/2)+1);
	botHistory = VectorCoord_malloc(((gridLenght*gridLenght)/2)+1);
	playerId = randint(0,1);
	i = 0;
	tictactoe_grid_display(grid);
	do {
		isWinner = tictactoe_play_as(grid, playerId, botHistory, humanHistory);
		i += 1;
		if ((! isWinner) && (i < (gridLenght * gridLenght))) {
			isWinner = tictactoe_play_as(grid, !playerId, botHistory, humanHistory);
			i += 1;
		}
	} while ((i < (gridLenght * gridLenght)) && (! isWinner));
	if (!isWinner) {
		puts("DRAW !");
	}
	puts("END GAME.");
	humanHistory = Vector_free(humanHistory);
	botHistory = Vector_free(botHistory);
	grid = tictactoe_grid_free(grid);
}
